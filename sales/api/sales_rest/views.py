from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    ShowSaleEncoder,
    ShowSalespersonEncoder,
    ShowCustomerEncoder,
)
from .models import AutomobileVO, Salesperson, Customer, Sale


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=ShowSaleEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            location_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=location_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            JsonResponse(
                {"message": "automobile vin invalid"},
                status=400,
            )
        try:
            location_phone = content["customer"]
            customer = Customer.objects.get(phone=location_phone)
            content["customer"] = customer
        except Customer.DoesNotExist:
            JsonResponse(
                {"message": "customer phone invalid"},
                status=400,
            )
        try:
            number = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_number=number)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            JsonResponse(
                {"message": "salesperson employee number invalid"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=ShowSaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def get_sale(request, id):
    if request.method == "GET":
        sale = Sale.objects.filter(id=id)
        return JsonResponse(
            {"sale": sale},
            encoder=ShowSaleEncoder,
        )

    elif request.method == "DELETE":
        count, _ = Sale.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=ShowSalespersonEncoder,
        )

    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=ShowSalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def get_salesperson(request, id):
    if request.method == "GET":
        salesperson = Salesperson.objects.filter(id=id)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=ShowSalespersonEncoder,
        )

    elif request.method == "DELETE":
        count, _ = Salesperson.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        try:
            content = json.loads(request.body)
            Salesperson.objects.filter(id=id).update(**content)
            return JsonResponse(
                Salesperson.objects.get(id=id),
                encoder=ShowSalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=ShowCustomerEncoder,
        )

    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=ShowCustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def get_customer(request, id):
    if request.method == "GET":
        customer = Customer.objects.filter(id=id)
        return JsonResponse(
            {"customer": customer},
            encoder=ShowCustomerEncoder,
        )

    elif request.method == "DELETE":
        count, _ = Customer.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        try:
            content = json.loads(request.body)
            Customer.objects.filter(id=id).update(**content)
            return JsonResponse(
                Customer.objects.get(id=id),
                encoder=ShowCustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )
