from django.urls import path
from .views import (
    list_customers,
    get_customer,
    list_sales,
    list_salespeople,
    get_salesperson,
    get_sale,
)

urlpatterns = [
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:id>/", get_customer, name="get_customer"),
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:id>/", get_salesperson, name="get_salesperson"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:id>/", get_sale, name="get_sale"),
]
