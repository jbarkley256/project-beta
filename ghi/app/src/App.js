import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import SalespersonForm from './sales/SalespersonForm';
import CustomerForm from './sales/CustomerForm';
import SaleForm from './sales/SaleForm';
import SalesList from './sales/SalesList';
import SalespersonHistory from './sales/SalespersonHistory';

import TechnicianForm from './service/TechniciansForm';
import AppointmentForm from './service/AppointmentForm';
import AppointmentsList from './service/AppointmentsList';
import ServiceHistory from './service/ServiceHistory';

import AutomobileForm from './inventory/AutomobileForm';
import ManufacturerForm from './inventory/ManufacturerForm';
import VehicleModelForm from './inventory/VehicleModelForm';
import AutomobileList from './inventory/AutomobileList';
import ManufacturerList from './inventory/ManufacturerList';
import VehicleModelList from './inventory/VehicleModelList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="sales">
            <Route path="newsalesperson" element={<SalespersonForm/>}/>
            <Route path="newcustomer" element={<CustomerForm/>}/>
            <Route path="newsale" element={<SaleForm/>}/>
            <Route path="saleslist" element={<SalesList/>}/>
            <Route path="salespersonhistory" element={<SalespersonHistory/>}/>
          </Route>
          <Route path="service">
            <Route path="newtechnician" element={<TechnicianForm />} />
            <Route path="newappointment" element={<AppointmentForm />} />
            <Route path="appointmentslist" element={<AppointmentsList />} />
            <Route path="servicehistory" element={<ServiceHistory />} />
          </Route>
          <Route path="inventory">
            <Route path="automobilelist" element={<AutomobileList/>}/>
            <Route path="manufacturerlist" element={<ManufacturerList/>}/>
            <Route path="vehiclemodellist" element={<VehicleModelList/>}/>
            <Route path="newautomobile" element={<AutomobileForm/>}/>
            <Route path="newmanufacturer" element={<ManufacturerForm/>}/>
            <Route path="newvehiclemodel" element={<VehicleModelForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
