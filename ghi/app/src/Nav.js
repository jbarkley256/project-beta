import { NavLink } from 'react-router-dom';
import NavDropdown from 'react-bootstrap/NavDropdown';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavDropdown
                id="nav-dropdown-dark-example"
                title="Inventory"
                menuVariant="dark">
                <NavDropdown.Item href="/inventory/manufacturerlist">View all manufacturers</NavDropdown.Item>
                <NavDropdown.Item href="/inventory/vehiclemodellist">View all vehicle models</NavDropdown.Item>
                <NavDropdown.Item href="/inventory/automobilelist">View all automobiles</NavDropdown.Item>
                <NavDropdown.Item href="/inventory/newmanufacturer">Add a manufacturer</NavDropdown.Item>
                <NavDropdown.Item href="/inventory/newvehiclemodel">Add a vehicle model</NavDropdown.Item>
                <NavDropdown.Item href="/inventory/newautomobile">Add an automobile</NavDropdown.Item>
              </NavDropdown>
            </li>
            <li className="nav-item">
              <NavDropdown
                id="nav-dropdown-dark-example"
                title="Sales"
                menuVariant="dark">
                <NavDropdown.Item href="/sales/saleslist">View all sales</NavDropdown.Item>
                <NavDropdown.Item href="/sales/salespersonhistory">View sales by salesperson</NavDropdown.Item>
                <NavDropdown.Item href="/sales/newsale">Add a sale</NavDropdown.Item>
                <NavDropdown.Item href="/sales/newcustomer">Add a customer</NavDropdown.Item>
                <NavDropdown.Item href="/sales/newsalesperson">Add a salesperson</NavDropdown.Item>
              </NavDropdown>
            </li>
            <li className="nav-item">
              <NavDropdown
                id="nav-dropdown-dark-example"
                title="Service"
                menuVariant="dark">
                <NavDropdown.Item href="/service/appointmentslist">View pending appointments</NavDropdown.Item>
                <NavDropdown.Item href="/service/newappointment">Add an appointment</NavDropdown.Item>
                <NavDropdown.Item href="/service/newtechnician">Add a technician</NavDropdown.Item>
                <NavDropdown.Item href="/service/servicehistory">Service History</NavDropdown.Item>
              </NavDropdown>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
