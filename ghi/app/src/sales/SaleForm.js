import {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

const SaleForm = () => {
  const [ formData, setFormData ] = useState({
    salesperson: '',
    customer: '',
    automobile: '',
    price: ''
  });

  const [ salespeople, setSalespeople ] = useState([]);
  const [ customers, setCustomers ] = useState([]);
  const [ autos, setAutomobiles ] = useState([]);
  const [error, setError] = useState(false);
  const [sent, setSent] = useState(false);

  useEffect(()=> {
    const loadData = async () => {
      let response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      } else {
        console.log("Failed to retrieve salespeople");
      }
      response = await fetch('http://localhost:8090/api/customers/');
      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
      } else {
        console.log("Failed to retrieve customers");
      }
      response = await fetch('http://localhost:8100/api/automobiles/');
      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        console.log("Failed to retrieve automobiles");
      }
    };

    loadData()
  }, []);

  const handleFormChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(formData)
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch('http://localhost:8090/api/sales/', fetchConfig);
    if (response.ok) {
      document.getElementById("form").reset()
      setSent(true)
      setTimeout(() => {
        setSent(false)
      }, 3000);
    } else {
      setSent(false)
      setError(true)
      setTimeout(() => {
          setError(false)
        }, 4000);
      }
    };

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            { error && <div className="alert alert-danger">Failed to add</div>}
            { sent && <div className="alert alert-success">Success!</div>}
            <h1>New sale entry</h1>
            <form onSubmit={handleSubmit} id="form">

                <div className="mb-3">
                    <select onChange={handleFormChange} name="salesperson" className="form-select">
                      <option>Choose a salesperson</option>
                      {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.employee_number} value={salesperson.employee_number}>{salesperson.name} (employee#{salesperson.employee_number})</option>
                        )
                        })}
                    </select>
                </div>

                <div className="mb-3">
                    <select onChange={handleFormChange} name="customer" className="form-select">
                        <option>Choose a customer</option>
                        {customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.phone}>{customer.name} (phone#{customer.phone})</option>
                        )
                        })}
                    </select>
                </div>

                <div className="mb-3">
                    <select onChange={handleFormChange} name="automobile" className="form-select">
                        <option>Choose an automobile</option>
                        {autos.map(automobile => {
                        return (
                            <option key={automobile.id} value={automobile.vin}>{automobile.model.name} (vin:{automobile.vin})</option>
                        )
                        })}
                    </select>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="..." name="price" className="form-control" />
                    <label htmlFor="price">Price</label>
                </div>

                <Link to="/sales/saleslist" className="btn btn-primary" style={{float: "right"}}>View all sales</Link>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
    </div>
    )
}

export default SaleForm;
