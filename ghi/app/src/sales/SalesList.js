import {useEffect, useState} from 'react';

const SalesList = () => {
  const [ sales, setSales ] = useState([]);

  useEffect(()=> {
    const loadData = async () => {
      let response = await fetch('http://localhost:8090/api/sales/');
      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      } else {
        console.log("Failed to retrieve sales");
      }
    };
    loadData()}, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>All sales</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                      {sales.map(sale =>
                              <tr key={sale.id}>
                                  <td>{sale.salesperson.name}</td>
                                  <td>{sale.customer.name}</td>
                                  <td>{sale.automobile.vin}</td>
                                  <td>{sale.price}</td>
                              </tr>
                          )}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    )
}

export default SalesList;
