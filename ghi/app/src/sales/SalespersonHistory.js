import {useEffect, useState} from 'react';

const SalespersonHistory = () => {

  const [ salespeople, setSalespeople ] = useState([]);
  const [ sales, setSales ] = useState([]);
  const [ selection, setSelection ] = useState("Choose a salesperson");

  useEffect(()=> {
    const loadData = async () => {
      let response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      } else {
        console.log("Failed to retrieve salespeople");
      }
      response = await fetch('http://localhost:8090/api/sales/');
      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      } else {
        console.log("Failed to retrieve sales");
      }
    };
    loadData()}, []);

  const handleFormChange = (e) => {
    setSelection(e.target.value)
  };

  const Filtering = (props) => {
    const {filtered} = props;

    return (
        <tr key={filtered.id}>
            <td>{filtered.salesperson.name}</td>
            <td>{filtered.customer.name}</td>
            <td>{filtered.automobile.vin}</td>
            <td>{filtered.price}</td>
        </tr>
  )
  };

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Salesperson history</h1>
                <div className="mb-3">
                    <select onChange={handleFormChange} name="salesperson" className="form-select">
                      <option>Choose a salesperson</option>
                        {salespeople.map(salesperson => (
                            <option key={salesperson.id} value={salesperson.employee_number}>{salesperson.name} (employee#{salesperson.employee_number})</option>
                        )
                        )}
                    </select>
                </div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Salesperson</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                      {sales.filter(sale => sale.salesperson.employee_number == selection || selection == "Choose a salesperson").map(filtered => {
                        if(!filtered) {
                          return (<tr><td><div className="alert alert-danger">Nothing to be seen here</div></td></tr>)} else {
                        return (<Filtering filtered={filtered}/>)}
                      })}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    )
}

export default SalespersonHistory;
