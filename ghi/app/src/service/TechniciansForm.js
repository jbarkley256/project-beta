import { useState } from "react";


const TechnicianForm = () => {
    const [ formData, setFormData ] = useState({
        first_name: '',
        last_name: '',
        employee_num: ''
    })

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const notfetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch('http://localhost:8080/api/technicians/', notfetchConfig);
        if (response.ok) {
            document.getElementById("form").reset();
        }
    }

    return (
        <div>
            <h1>New Technician Entry</h1>
            <form onSubmit={ handleSubmit } id="form">

                <div>
                    <label>First Name</label>
                    <input onChange={ handleFormChange } placeholder="..." type="text" name="first_name" className="form-control" />
                </div>

                <div>
                    <label>Last Name</label>
                    <input onChange={ handleFormChange } placeholder="..." type="text" name="last_name" className="form-control" />
                </div>

                <div>
                    <label>Employee Number</label>
                    <input onChange={ handleFormChange } placeholder="..." type="number" name="employee_num" className="form-control" />
                </div>

                <br></br>
                <button>Add</button>

            </form>
        </div>
    )
}

export default TechnicianForm;
