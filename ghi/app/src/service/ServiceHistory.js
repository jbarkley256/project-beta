import { useState, useEffect } from 'react';


const ServiceHistory = () => {
    const [ appointments, setAppointments ] = useState([]);
    const [ filterInput, setFilterInput ] = useState("");

    useEffect(() => {
        const loadData = async () => {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            } else {
                console.error("Failed to fetch appointments");
            }
        }
        loadData();
    }, [])

    const handleFilterChange = (e) => {
        setFilterInput(e.target.value);
    }

    return (
        <div>
            <h1>Service History</h1>
            <div>
                <input onChange={ handleFilterChange } name="vin" />
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Date</th>
                        <th>Problem</th>
                        <th>Technician</th>
                        <th>VIP?</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    { appointments.filter(appointment => appointment.vin.includes(filterInput) ).map(filtered => (
                        <tr key={ filtered.id }>
                            <td>{ filtered.customer.name }</td>
                            <td>{ filtered.vin }</td>
                            <td>{ filtered.date }</td>
                            <td>{ filtered.problem }</td>
                            <td>{ filtered.technician.first_name } { filtered.technician.last_name }</td>
                            <td>
                                { filtered.vip === true ? <div>YES!</div> : <div></div> }
                            </td>
                            <td>{ filtered.status }</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )

}

export default ServiceHistory;
