import { useEffect, useState } from "react";


const AppointmentForm = () => {
    const [ formData, setFormData ] = useState({
        customer: '',
        vin: '',
        date: '',
        problem: '',
        technician: ''
    })

    const [ technicians, setTechnicians ] = useState([]);
    const [ customers, setCustomers ] = useState([]);

    useEffect(() => {
        const loadData = async () => {
            let response = await fetch('http://localhost:8080/api/technicians/');

            if (response.ok) {
                const data = await response.json();
                setTechnicians(data.technicians);
            } else {
                console.log("Failed to retrieve technicians");
            }
            response = await fetch('http://localhost:8090/api/customers/');

            if (response.ok) {
                const data = await response.json();
                setCustomers(data.customers);
            } else {
                console.log("Failed to retrieve customers");
            }
        }

        loadData();
    }, [])

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch('http://localhost:8080/api/appointments/', fetchConfig);
        if (response.ok) {
            document.getElementById("form").reset();
        }
    }

    return (
        <div>
            <h1>New Appointment Entry</h1>
            <form onSubmit={ handleSubmit } id="form">

                <div>
                    <label>Choose a customer</label>
                    <select onChange={ handleFormChange } name="customer" className="form-control">
                        <option>...</option>
                        { customers.map(customer => {
                            return (
                                <option key={ customer.id } value={ customer.id }>{ customer.name }</option>
                            )
                        })}
                    </select>
                </div>

                <div>
                    <label>VIN</label>
                    <input onChange={ handleFormChange } placeholder="..." type="text" name="vin" className="form-control" />
                </div>

                <div>
                    <label>Date</label>
                    <input onChange={ handleFormChange } type="datetime-local" name="date" className="form-control" />
                </div>

                <div>
                    <label>Problem</label>
                    <input onChange={ handleFormChange } placeholder="..." type="text" name="problem" className="form-control" />
                </div>

                <div>
                    <label>Choose a technician</label>
                    <select onChange={ handleFormChange } name="technician" className="form-control">
                        <option>...</option>
                        { technicians.map(technician => {
                            return (
                                <option key={ technician.id } value={ technician.id }>{ technician.first_name } { technician.last_name }</option>
                            )
                        })}
                    </select>
                </div>

                <br></br>
                <button>Add</button>

            </form>
        </div>
    )
}

export default AppointmentForm;
