import { useState, useEffect } from 'react';


const AppointmentsList = () => {
    const [ appointments, setAppointments ] = useState([]);

    useEffect(() => {
        const loadData = async () => {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const data = await response.json();
                setAppointments(data.appointments);
            } else {
                console.error("Failed to fetch appointments");
            }
        }
        loadData();
    }, [])

    const handleChange = async (id, param) =>{
        await fetch(`http://localhost:8080/api/appointments/${id}/`, {
            method: "PUT",
            body: JSON.stringify(param),
            headers: {
                'Content-Type': 'application/json',
            }
        })
        window.location.reload();
    }


    return (
        <div>
            <h1>Pending Appointments</h1>
            <table>
                <thead>
                    <tr>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Date</th>
                        <th>Problem</th>
                        <th>Technician</th>
                        <th>VIP?</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    { appointments.filter(appointment => appointment.status === "Open").map(filtered => (
                        <tr key={ filtered.id }>
                            <td>{ filtered.customer.name }</td>
                            <td>{ filtered.vin }</td>
                            <td>{ filtered.date }</td>
                            <td>{ filtered.problem }</td>
                            <td>{ filtered.technician.first_name } { filtered.technician.last_name }</td>
                            <td>
                                { filtered.vip === true ? <div>YES!</div> : <div></div> }
                            </td>
                            <td>
                            <button onClick={ () => handleChange(filtered.id, { 'status': 'Completed' }) }>Completed</button>
                            </td>
                            <td>
                                <button onClick={ () => handleChange(filtered.id, { 'status': 'Canceled' }) }>Cancel</button>
                            </td>
                        </tr>
                        ))
                    }
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentsList;
