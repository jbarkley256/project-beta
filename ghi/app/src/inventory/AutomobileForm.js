import {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

const AutomobileForm = () => {
  const [ formData, setFormData ] = useState({
    color: '',
    year: '',
    vin: '',
    model_id: ''
  });

  const [ models, setModels ] = useState([]);
  const [error, setError] = useState(false);
  const [sent, setSent] = useState(false);

  useEffect(()=> {
    const loadData = async () => {
      let response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      } else {
        console.log("Failed to retrieve vehicle models");
      }
    };

    loadData()
  }, []);

  const handleFormChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch('http://localhost:8100/api/automobiles/', fetchConfig);
    if (response.ok) {
      document.getElementById("form").reset();
      setSent(true)
      setTimeout(() => {
          setSent(false)
        }, 3000);
    } else {
      setSent(false)
      setError(true)
      setTimeout(() => {
          setError(false)
        }, 4000);
    }
  };

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            { error && <div className="alert alert-danger">Failed to add</div>}
            { sent && <div className="alert alert-success">Success!</div>}
            <h1>New automobile entry</h1>
            <form onSubmit={handleSubmit} id="form">

                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="..." name="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="..." name="year" className="form-control" />
                    <label htmlFor="year">Year</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="..." name="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                </div>

                <div className="mb-3">
                    <select onChange={handleFormChange} name="model_id" className="form-select">
                        <option>Choose a model</option>
                        {models.map(model => {
                        return (
                            <option key={model.id} value={model.id}>{model.name}</option>
                        )
                        })}
                    </select>
                </div>

                <Link to="/inventory/automobilelist" className="btn btn-primary" style={{float: "right"}}>List all autos</Link>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
    </div>
    )
}

export default AutomobileForm;
