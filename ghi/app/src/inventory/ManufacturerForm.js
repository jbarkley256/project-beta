import {useState} from 'react';
import { Link } from 'react-router-dom';

const ManufacturerForm = () => {
    const [formData, setFormData] = useState({
        name: ''
    });

    const [error, setError] = useState(false);
    const [sent, setSent] = useState(false);

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        };

        const response = await fetch("http://localhost:8100/api/manufacturers/", fetchConfig);
        if (response.ok) {
            document.getElementById("form").reset()
            setSent(true)
            setTimeout(() => {
                setSent(false)
              }, 3000);
        } else {
            setSent(false)
            setError(true)
            setTimeout(() => {
                setError(false)
              }, 4000);
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-3 mt-4">
                    { error && <div className="alert alert-danger">Failed to add</div>}
                    { sent && <div className="alert alert-success">Success!</div>}
                    <h1>New manufacturer entry</h1>
                    <form onSubmit={handleSubmit} id="form">

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="..." type="text" name="name" className="form-control"/>
                            <label>Name</label>
                        </div>

                    <Link to="/inventory/manufacturerlist" className="btn btn-primary" style={{float: "right"}}>Manufacturer list</Link>
                    <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )

}

export default ManufacturerForm;
