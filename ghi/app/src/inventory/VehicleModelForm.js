import {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

const SaleForm = () => {
  const [ formData, setFormData ] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: ''
  });

  const [manufacturers, setManufacturer] = useState([]);
  const [error, setError] = useState(false);
  const [sent, setSent] = useState(false);

  useEffect(()=> {
    const loadData = async () => {
      let response = await fetch('http://localhost:8100/api/manufacturers/');
      if (response.ok) {
        const data = await response.json();
        setManufacturer(data.manufacturers);
      } else {
        console.log("Failed to retrieve manufacturers");
      }
    };

    loadData()
  }, []);

  const handleFormChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    })
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(formData)
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch('http://localhost:8100/api/models/', fetchConfig);
    if (response.ok) {
      document.getElementById("form").reset()
      setSent(true)
      setTimeout(() => {
          setSent(false)
        }, 3000);
    } else {
        setSent(false)
        setError(true)
        setTimeout(() => {
            setError(false)
          }, 4000);
    };
  };

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            { error && <div className="alert alert-danger">Failed to add</div>}
            { sent && <div className="alert alert-success">Success!</div>}
            <h1>New vehicle model entry</h1>
            <form onSubmit={handleSubmit} id="form">

                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="..." name="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>

                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="..." name="picture_url" className="form-control" />
                    <label htmlFor="picture_url">Picture URL</label>
                </div>

                <div className="mb-3">
                    <select onChange={handleFormChange} name="manufacturer_id" className="form-select">
                        <option>Choose a manufacturer</option>
                        {manufacturers.map(manufacturer => {
                        return (
                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                        )})}
                    </select>
                </div>

                <Link to="/inventory/vehiclemodellist" className="btn btn-primary" style={{float: "right"}}>Vehicle models list</Link>
                <button className="btn btn-primary">Add</button>
            </form>
            </div>
        </div>
    </div>
    )
}

export default SaleForm;
