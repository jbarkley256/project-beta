import {useEffect, useState} from 'react';

const ManufacturerList = () => {
  const [ manufacturers, setManufacturers ] = useState([]);

  useEffect(()=> {
    const loadData = async () => {
      let response = await fetch('http://localhost:8100/api/manufacturers/');
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      } else {
        console.log("Failed to retrieve manufacturers");
      }
    }
    loadData()}, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>All manufacturers</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {manufacturers?.map(manufacturer =>
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    )
}

export default ManufacturerList;
