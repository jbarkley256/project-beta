import {useEffect, useState} from 'react';

const VehicleModelList = () => {
  const [ models, setModels ] = useState([]);

  useEffect(()=> {
    const loadData = async () => {
      let response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      } else {
        console.log("Failed to retrieve vehicle models");
      }
    };
    loadData()}, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>All vehicle models</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {models.map(vehicle =>
                            <tr key={vehicle.id}>
                                <td>{vehicle.name}</td>
                                <td>{vehicle.manufacturer.name}</td>
                                <td><img src={vehicle.picture_url} alt="car"  style={{"width": "100px"}}></img></td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    )
}

export default VehicleModelList;
