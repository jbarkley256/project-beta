# CarCar

Team:

* Jeremy Barkley - Service microservice
* Gavin Andrew - Sales microservice

## Design
There are three microservices, the sales, service, and inventory microservices.
The sales and service microservices will use VOs and pollers to get information
from the inventory microservice to populate their own models.
The data we collect and store in our microservices will be displayed though a React front-end.

## Service microservice

The Service microservice has Technician and Appointment models for maintaining and creating respective data. It also contains an AutomobileVO model which is used to compare the VIN numbers from the Inventory microservice so VIP treatment can be offered to returning customers. A CustomerVO model, connected to the Sales microservice, was added so there could be consistency between Sales and Service customers.

## Sales microservice

I have four models, Salesperson, Customer, Sale and AutomobileVO.
Salesperson and Customer each with their own respective attributes regarding
the person in question act as foreign keys to the Sale model, which additionally has
an attribute price and another foreign key AutomobileVO. AutomobileVO is a model
that is populated with the href and vin of the Automobiles stored in the
Inventory API through a poller.
