from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)


class CustomerVO(models.Model):
    import_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=50)


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_num = models.IntegerField()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        ordering = ("first_name", "last_name")


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    date = models.DateTimeField()
    problem = models.TextField()
    vip = models.BooleanField(default=False)
    status = models.CharField(max_length=10, default="Open")

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    customer = models.ForeignKey(
        CustomerVO,
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.vin
