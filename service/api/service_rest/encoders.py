from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO, CustomerVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin"]


class CustomerVOEncoder(ModelEncoder):
    model = CustomerVO
    properties = ["import_id", "name"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_num",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date",
        "vip",
        "technician",
        "customer",
        "vin",
        "problem",
        "status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "customer": CustomerVOEncoder(),
    }
